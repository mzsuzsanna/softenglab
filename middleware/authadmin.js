module.exports = (request, response, next) => {
  if (request.session.role === 'admin') {
    next();
  } else {
    response.status(401).render('error', {
      message: 'Ez a funkció csak tanár számára elérhető.', link: '/api/users/login', clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username,
    });
  }
};
