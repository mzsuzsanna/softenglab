exports.hasProps = (propNames) => (req, res, next) => {
  const nonExistentProps = propNames.filter((propName) => !(propName in req.fields));

  if (nonExistentProps.length === 0) {
    next();
  } else {
    res.status(400).render('error', { message: `The following fields are missing: ${nonExistentProps.join(', ')}` });
  }
};
