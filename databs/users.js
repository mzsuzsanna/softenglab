const execute = require('./config');

execute(`
CREATE TABLE IF NOT EXISTS users (
userName varchar(25),
email VARCHAR(50),
userPassword VARCHAR(100),
userRole VARCHAR(10)
);
`);

exports.insertUser = (user) => execute('INSERT INTO users VALUES (?, ?, ?, ?)', [user.username, user.email, user.hashWithSalt, user.roleName])
  .then((result) => result.affectedRows > 0);

exports.userExists = (userName) => execute('SELECT COUNT(*) AS count FROM users WHERE userName=?', [userName])
  .then((result) => (result[0].count > 0));

exports.selectUser = (userName) => execute('SELECT userPassword, userRole FROM users WHERE userName=?', [userName])
  .then((result) => ({ userPassword: result[0].userPassword, userRole: result[0].userRole }));
