const execute = require('./config');

execute(`
CREATE TABLE IF NOT EXISTS users (
userName varchar(25),
email VARCHAR(50),
userPassword VARCHAR(100),
userRole VARCHAR(10)
);
`);

exports.selectSubjects = () => execute('SELECT * FROM subjects;');

exports.selectFirstSubject = () => execute('SELECT subjectCode FROM subjects LIMIT 1');

exports.findSubjectByCode = (subjectCode) => execute('SELECT * FROM subjects WHERE subjectCode=?', [subjectCode])
  .then((subjects) => subjects[0]);

exports.findSubjectByOwner = (owner) => execute('SELECT * FROM subjects WHERE owner=?', [owner]);

exports.selectDescription = (subjectCode) => execute('SELECT sDescription FROM subjects WHERE subjectCode=?', [subjectCode])
  .then((result) => ({ sDescription: result.sDescription }));

exports.subjectExists = (subjectCode) => execute('SELECT COUNT(*) AS count FROM subjects WHERE subjectCode=?', [subjectCode])
  .then((result) => (result[0].count > 0));

exports.insertSubject = (subject) => {
  return execute('INSERT INTO subjects VALUES (?, ?, ?)', [subject.owner, subject.subjectCode, subject.sDescription])
    .then((result) => ({
      subjectCode: result.subjectCode,
      owner: subject.owner,
      Description: subject.sDescription,
    }));
};

exports.updateSubject = (subjectCode, subject) => execute('UPDATE subjects SET ? WHERE subjectCode=?', [subject, subjectCode])
  .then((result) => result.affectedRows > 0);

exports.addSubject = (userName, subject) => execute('INSERT INTO subjects VALUES (?, ?, ?)', [userName, subject.subjectcode, subject.subjectdescription])
  .then((result) => result.affectedRows > 0);

exports.deleteSubject = (subjectCode) => execute('DELETE FROM subjects WHERE subjectCode=?', [subjectCode])
  .then((result) => result.affectedRows > 0);