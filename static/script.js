function showDescription(subjectCode, row) {
    var tableref = document.getElementById('myTable');

    if(document.contains(document.getElementById(`desc${subjectCode}`))) {
        celltext = document.getElementById(`desc${subjectCode}`);
    } else {
        var tr = tableref.insertRow(row.parentNode.rowIndex+1);
        var celltitle =  tr.insertCell(0);
        var celltext = tr.insertCell(1);
        celltext.setAttribute("colspan", "3");
        celltitle.innerHTML = "Leírás";   
        celltitle.style.fontWeight = "bold"; 
        celltext.setAttribute("id", `desc${subjectCode}`);
    }

    fetch(`/api/subjects/${subjectCode}/description`, {
        method: 'POST',
    })
    .then(response => response.json()) 
    .then(answer => answer.map(answer => `${answer.sDescription}`))
    .then((formattedAnswer) => {
        celltext.innerHTML = formattedAnswer;
      });

}

function deleteAssignment(event, assignmentCode, row) {

    event.preventDefault();

    fetch(`/api/assignments/:${assignmentCode}`, { method: 'DELETE', })
    .then(response => response.json()) 
    .then(answer => answer.map(answer => `${answer.error}`))
    .then((formattedAnswer) => { 
        if (formattedAnswer == false) {
            alert('A törlés sikeretelen volt.'); 
        } else {
            alert('A törlés sikeres volt.');
            var ind = row.parentNode.parentNode.rowIndex;
            document.getElementById('myTable').deleteRow(ind); 
        }
    });
}
