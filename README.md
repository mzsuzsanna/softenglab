# Webprogramozás laborfeladatok

## 4.Laborházi - használati útmutató

### Adatbázis előkészítése

#### Felhasználó és adatbázis létrehozása

* [setup.sql](/setup.sql)

#### Táblák létrehozása és feltöltése

A táblalétrehozás js kódban is megtörténik, ha erre nem volt alkalom előzetesen.

* [insertdata.sql](/insertdata.sql)

### Oldalak megjelenítése

*tanár/admin funkciói: tantárgy törlése, létrehozása, feladat hozzáadása*

*student funkciói: tantárgyhoz tartozó feladatok listázása*

#### Főoldal, tantárgyak elérése és törlése

* <http://localhost:8080/main>
* <http://localhost:8080/>

#### Tantárgy hozzáadása

* <http://localhost:8080/submitsubject>

#### Feladatok elérése és hozzáadása

* <http://localhost:8080/:Tantárgykód>

Pédául: <http://localhost:8080/82927>

#### Regiszztráció

* [http://localhost:8080/signup](http://localhost:8080/signup)

#### Bejelentkezés

* [http://localhost:8080/login](http://localhost:8080/login)