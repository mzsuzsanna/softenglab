const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const fullUrlMiddleware = require('../middleware/fullurl');
const assignmentRoutes = require('./assignments');
const subjectRoutes = require('./subjects');
const userRoutes = require('./users');
const subjectDao = require('../databs/subjects');

const router = express.Router();

router.post('/showDescription', (request, response) => {
  subjectDao.selectDescription(request)
    .then((description) => {
      console.log(description);
      return response.json(description);
    })
    .catch((err) => response.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

router.use(morgan('tiny'));
router.use(fullUrlMiddleware);
router.use(bodyParser.json());

router.use('/assignments', assignmentRoutes);
router.use('/users', userRoutes);
router.use('/subjects', subjectRoutes);

module.exports = router;
